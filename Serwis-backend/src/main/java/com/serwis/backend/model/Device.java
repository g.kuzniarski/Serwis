package com.serwis.backend.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "Device")
public class Device implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDevice;

    @NotEmpty
    @Column(name = "Name", nullable = false)
    private String name;

    @Column(name = "Category", nullable = false)
    @Enumerated(EnumType.STRING)
    private dicCategory category;

    @Column(name = "State", nullable = false)
    @Enumerated(EnumType.STRING)
    private dicState state;

    @Column(name = "Comment")
    private String comment;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "idParameter")
    private List<Parameter> parameters;

    public Long getIdDevice() {
        return idDevice;
    }

    public void setIdDevice(Long idDevice) {
        this.idDevice = idDevice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public dicCategory getCategory() {
        return category;
    }

    public void setCategory(dicCategory category) {
        this.category = category;
    }

    public dicState getState() {
        return state;
    }

    public void setState(dicState state) {
        this.state = state;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }
}

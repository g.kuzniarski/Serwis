package com.serwis.backend.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Parameter")
public class Parameter implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idParameter;

    @NotEmpty
    @Column(name = "Name")
    private String name;

    @NotEmpty
    @Column(name = "Value")
    private String value;

    public long getIdParameter() {
        return idParameter;
    }

    public void setIdParameter(long idParameter) {
        this.idParameter = idParameter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

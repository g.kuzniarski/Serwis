package com.serwis.backend.controller;

import com.serwis.backend.model.Device;
import com.serwis.backend.service.DeviceService;
import com.serwis.backend.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/api")
public class DeviceController {

    private static final Logger logger = LoggerFactory.getLogger(DeviceController.class);

    @Autowired
    DeviceService deviceService;

    @RequestMapping(value = "/devices/", method = RequestMethod.GET)
    public ResponseEntity<List<Device>> listAllDevices() {
        List<Device> devices = deviceService.findAllDevices();
        if (devices.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(devices, HttpStatus.OK);
    }

    @RequestMapping(value = "/device/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getDevice(@PathVariable("id") long id) {
        logger.info("Fetching Device with id {}", id);
        Device device = deviceService.findById(id);
        if (device == null) {
            logger.error("Device with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Device with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(device, HttpStatus.OK);
    }

    @RequestMapping(value = "/device/", method = RequestMethod.POST)
    public ResponseEntity<?> createDevice(@RequestBody Device device, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Device : {}", device);

        if (device.getIdDevice() != null && deviceService.isDeviceExist(device)) {
            logger.error("Unable to create. A Device with name {} already exist", device.getName());
            return new ResponseEntity<>(new CustomErrorType("Unable to create. A Device with name "
                    + device.getName() + " already exist."), HttpStatus.CONFLICT);
        }

        deviceService.saveDevice(device);

        return new ResponseEntity<>(device, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/device/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateDevice(@PathVariable("id") long id, @RequestBody Device device) {
        logger.info("Updating Device with id {}", id);

        Device currentDevice = deviceService.findById(id);

        if (currentDevice == null) {
            logger.error("Unable to update. Device with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Device with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        deviceService.updateDevice(device);
        return new ResponseEntity<>(currentDevice, HttpStatus.OK);
    }

    @RequestMapping(value = "/device/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteDevice(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting Device with id {}", id);

        Device device = deviceService.findById(id);
        if (device == null) {
            logger.error("Unable to delete. Device with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. Device with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        deviceService.deleteDeviceById(id);
        return new ResponseEntity<Device>(HttpStatus.NO_CONTENT);
    }
}

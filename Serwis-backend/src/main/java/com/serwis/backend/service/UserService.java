package com.serwis.backend.service;

import com.serwis.backend.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    public User findUserByUsername(String email);

    public void saveUser(User user);
}
package com.serwis.backend.service;

import com.serwis.backend.model.Device;
import com.serwis.backend.repositories.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("deviceService")
@Transactional
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private DeviceRepository deviceRepository;

    @Override
    public Device findById(Long id) {
        return deviceRepository.findOne(id);
    }

    @Override
    public Device findByName(String name) {
        return deviceRepository.findByName(name);
    }

    @Override
    public void saveDevice(Device device) {
        deviceRepository.save(device);
    }

    @Override
    public void updateDevice(Device device) {
        saveDevice(device);
    }

    @Override
    public void deleteDeviceById(Long id) {
        deviceRepository.delete(id);
    }

    @Override
    public void deleteAllDevices() {
        deviceRepository.deleteAll();
    }

    @Override
    public List<Device> findAllDevices() {
        return deviceRepository.findAll();
    }

    @Override
    public boolean isDeviceExist(Device device) {
        return findById(device.getIdDevice()) != null;
    }

}

package com.serwis.backend.service;

import com.serwis.backend.model.Device;

import java.util.List;

public interface DeviceService {

    Device findById(Long id);

    Device findByName(String name);

    void saveDevice(Device device);

    void updateDevice(Device device);

    void deleteDeviceById(Long id);

    void deleteAllDevices();

    List<Device> findAllDevices();

    boolean isDeviceExist(Device device);
}

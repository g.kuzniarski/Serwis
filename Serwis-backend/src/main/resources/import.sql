INSERT INTO Role (name) VALUES ('ROLE_ADMIN');
INSERT INTO Role (name) VALUES ('ROLE_USER');
INSERT INTO serwisuser (active, username, password) VALUES (1, 'admin', '$2a$10$0V0eDVccQS1AFfl8L1U45eHkmS0keocTaoqu45kaO6obyd.JzRir.');
INSERT INTO user_role (user_id, role_id) VALUES (1, 1);
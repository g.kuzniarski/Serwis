import {Component, OnInit} from '@angular/core';
import {LoginService} from '../services/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  isUserLogged = false;
  errorMsg = '';

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit() {
    this.loginService.userLogged.subscribe(isLogged => {
      this.isUserLogged = isLogged;
    });
    this.isUserLogged = this.loginService.isUserLogged();
    if (this.isUserLogged) {
      this.onLogout();
    }
  }

  onLogin() {
    this.errorMsg = '';
    this.loginService.login(this.username, this.password).subscribe(r => {
      // sessionStorage.setItem('userRole', r);
      sessionStorage.setItem('username', this.username);
    }); // , err => this.errorMsg = 'Błąd logowania');
  }

  onRegister() {
    this.errorMsg = '';
    this.loginService.register(this.username, this.password).subscribe(val => {
      this.loginService.login(this.username, this.password).subscribe(r => {
        // sessionStorage.setItem('userRole', r);
        sessionStorage.setItem('username', this.username);
      });
    }); // , err => this.errorMsg = 'Błąd rejestracji');
  }

  onLogout() {
    this.loginService.logout();
  }
}

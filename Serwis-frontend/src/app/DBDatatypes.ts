export interface Device {
  idDevice?: number;
  name: string;
  category: string;
  state: string;
  comment: string;
  parameters: Parameter[];
}

export interface Parameter {
  idParameter?: number;
  name: string;
  value: string;
}

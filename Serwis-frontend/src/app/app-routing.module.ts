import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContactComponent} from './contact/contact.component';
import {PageNotFoundComponent} from './page-not-found.component';
import {ContactModule} from './contact/contact.module';
import {LoginModule} from './login/login.module';
import {LoginComponent} from './login/login.component';
import {LoginGuard} from './login/login.guard';
import {DeviceComponent} from './device/device.component';
import {DeviceModule} from './device/device.module';

export const BASE_ROUTE = {
  device: {
    label: 'Sprzęt',
    routerLink: 'device',
    icon: 'fa-folder-open'
  },
  contact: {
    label: 'Kontakt',
    routerLink: 'contact',
    icon: 'fa fa-envelope'
  },
  login: {
    label: 'Logowanie',
    routerLink: 'login',
    icon: 'fa fa-user'
  }
};

const routes: Routes = [
  {
    path: '',
    redirectTo: BASE_ROUTE.device.routerLink,
    pathMatch: 'full'
  },
  {
    path: BASE_ROUTE.login.routerLink,
    component: LoginComponent
  },
  {
    path: BASE_ROUTE.device.routerLink,
    component: DeviceComponent,
    canActivate: [LoginGuard]
  },
  {
    path: BASE_ROUTE.contact.routerLink,
    component: ContactComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    DeviceModule,
    ContactModule,
    LoginModule
  ],
  exports: [RouterModule],
  declarations: [PageNotFoundComponent],
  providers: []
})
export class AppRoutingModule {
}

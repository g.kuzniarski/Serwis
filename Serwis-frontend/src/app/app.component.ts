import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/primeng';
import {ActivatedRoute, Router} from '@angular/router';
import {BASE_ROUTE} from './app-routing.module';
import {LoginService} from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  items: MenuItem[];
  activeItem: MenuItem;
  isUserLogged = false;
  btnLoginText = 'Zaloguj się';

  constructor(private router: Router, private route: ActivatedRoute,
              private loginService: LoginService) {
  }

  ngOnInit(): void {

    const routeItems = Object.values(BASE_ROUTE) as MenuItem[];
    this.items = routeItems.map(i => {
      return {
        label: i.label,
        icon: i.icon,
        routerLink: i.routerLink
      };
    });

    this.items.forEach(item => {
      if (item.routerLink === 'device') {
        this.loginService.userLogged.subscribe(r => item.disabled = !r);
      } else if (item.label === 'Logowanie' || item.label === 'Wylogowanie') {
        this.loginService.loginButtonLabel.subscribe(r => item.label = r);
        this.loginService.loginButtonIcon.subscribe(r => item.icon = r);
      }
    });

    this.loginService.setUserState();
    this.activeItem = this.items[this.items.findIndex(val => this.router.url.includes(val.routerLink))];
  }
}

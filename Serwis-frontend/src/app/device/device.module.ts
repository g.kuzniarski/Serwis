import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DeviceComponent} from './device.component';
import {LoginGuard} from '../login/login.guard';
import {
  AutoCompleteModule, ButtonModule, CalendarModule, DataTableModule, DialogModule, DropdownModule, SharedModule,
  TooltipModule
} from 'primeng/primeng';
import {DeviceService} from '../services/device.service';
import {LoginService} from '../services/login.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    DialogModule,
    BrowserAnimationsModule,
    TooltipModule,
    ButtonModule,
    FormsModule,
    SharedModule,
    AutoCompleteModule,
    DropdownModule,
    CalendarModule
  ],
  declarations: [DeviceComponent],
  exports: [DeviceComponent],
  providers: [DeviceService, LoginService, LoginGuard]
})
export class DeviceModule {
}

import {Component, EventEmitter, OnInit} from '@angular/core';
import {DeviceService} from '../services/device.service';
import {Device} from '../DBDatatypes';
import {Observable} from 'rxjs/Observable';
import {SelectItem} from 'primeng/primeng';
import {Dictionary} from '../dictionary';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnInit {

  displayDialog: boolean;
  device: Device;
  selectedDevice: Device;
  devices$: Observable<Device[]>;
  devicesEmitter: EventEmitter<any> = new EventEmitter();
  states: SelectItem[] = [];
  categories: SelectItem[] = [];

  constructor(private deviceService: DeviceService) {
  }

  ngOnInit() {
    this.devices$ = this.devicesEmitter;
    this.refreshDevices();
    this.refreshDeviceStates();
    this.refreshDeviceCategories();
  }

  showDialogToAdd() {
    this.device = this.createEmptyDevice();
    this.displayDialog = true;
  }

  hideDialog() {
    this.displayDialog = false;
  }

  refreshDevices() {
    this.deviceService.getDevices()
      .subscribe(r => {
        this.devicesEmitter.emit(r);
      });
  }

  refreshDeviceStates() {
    Dictionary.DEVICE_STATES.forEach(state => {
      this.states.push({
        value: state,
        label: state
      });
    });
  }

  refreshDeviceCategories() {
    Dictionary.DEVICE_CATEGORIES.forEach(category => {
      this.categories.push({
        value: category,
        label: category
      });
    });
  }

  saveDevice() {
    if (this.device.idDevice) {
      this.deviceService.updateDevice(this.device).subscribe(r => {
        console.log(r);
        this.refreshDevices();
        this.displayDialog = false;
      });
    } else {
      this.deviceService.createDevice(this.device).subscribe(r => {
        this.refreshDevices();
        this.displayDialog = false;
      });
    }
  }

  deleteDevice() {
    this.deviceService.deleteDevice(this.device.idDevice).subscribe(r => {
      this.refreshDevices();
      this.displayDialog = false;
    });
  }

  onRowSelect(event) {
    this.device = this.cloneDevice(event.data);
    this.displayDialog = true;
  }

  cloneDevice(deviceToClone: Device): Device {
    const clonedDevice = this.createEmptyDevice();
    for (const prop in deviceToClone) {
      if (clonedDevice[prop] instanceof Array) {
        clonedDevice[prop] = [...deviceToClone[prop]];
      } else {
        clonedDevice[prop] = deviceToClone[prop];
      }
    }
    return clonedDevice;
  }

  createEmptyDevice(): Device {
    return {
      name: null,
      category: null,
      comment: null,
      parameters: [],
      state: null
    };
  }

  addParameter() {
    const param = [...this.device.parameters];
    param.push({
      name: 'nazwa',
      value: 'wartość'
    });
    this.device.parameters = param;
  }

  deleteParameter(param: number) {
    const tmp = [...this.device.parameters];
    tmp.splice(param, 1);
    this.device.parameters = tmp;
  }
}


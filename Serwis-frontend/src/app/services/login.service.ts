import {EventEmitter, Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {FetchApiService} from './fetch-api.service';
import {Router} from '@angular/router';
import {BASE_ROUTE} from '../app-routing.module';

const LOGIN_PATH = '/api/login';
const REGISTER_PATH = '/api/register';

@Injectable()
export class LoginService extends FetchApiService {
  userLogged: EventEmitter<boolean> = new EventEmitter();
  loginButtonLabel: EventEmitter<string> = new EventEmitter();
  loginButtonIcon: EventEmitter<string> = new EventEmitter();

  constructor(public http: Http,
              private router: Router) {
    super(http);
  }

  login(username: string, password: string) {
    const body = {
      username: username,
      password: password,
    };

    return this.post(LOGIN_PATH, body);
  }

  register(username: string, password: string) {
    const body = {
      username: username,
      password: password,
    };
    return this.post(REGISTER_PATH, body);
  }

  logout() {
    sessionStorage.clear();
    this.router.navigate([BASE_ROUTE.login.routerLink]);
  }

  isUserLogged(): boolean {
    return !!(sessionStorage.getItem('userRole'));
  }
}

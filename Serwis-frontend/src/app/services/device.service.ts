import {Injectable} from '@angular/core';
import {FetchApiService} from './fetch-api.service';
import {Observable} from 'rxjs/Observable';
import {Device} from '../DBDatatypes';

import 'rxjs/add/observable/of';

const DEVICE_URL = '/api/device';

@Injectable()
export class DeviceService extends FetchApiService {

  getDevices(): Observable<Device[]> {
    return this.get(`${DEVICE_URL}s/`);
  }

  getDevice(idDevice: number): Observable<Device> {
    return this.get(`${DEVICE_URL}/${idDevice}`);
  }

  createDevice(device: Device) {
    return this.post(`${DEVICE_URL}/`, device);
  }

  deleteDevice(idDevice: number) {
    return this.del(`${DEVICE_URL}/${idDevice}`);
  }

  updateDevice(device: Device) {
    return this.put(`${DEVICE_URL}/${device.idDevice}`, device);
  }
}

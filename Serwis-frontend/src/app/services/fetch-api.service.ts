import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

@Injectable()
export class FetchApiService {

  constructor(public http: Http) {
  }

  get(apiPath, options?: RequestOptions, headers: Headers = null) {
    return this.http.get(apiPath, this.setupHeader(options, headers))
      .map(this.extractData)
      .share()
      .catch(this.handleError);
  }

  post(apiPath, body: any = {}, options?: RequestOptions, headers: Headers = null) {
    return this.http.post(apiPath, body, this.setupHeader(options, headers))
      .map(this.extractData)
      .share()
      .catch(this.handleError);
  }

  del(apiPath, options?: RequestOptions, headers: Headers = null) {
    return this.http.delete(apiPath, this.setupHeader(options, headers))
      .map(this.extractData)
      .share()
      .catch(this.handleError);
  }

  put(apiPath, body: any = {}, options?: RequestOptions, headers: Headers = null) {
    return this.http.put(apiPath, body, this.setupHeader(options, headers))
      .map(this.extractData)
      .share()
      .catch(this.handleError);
  }

  private setupHeader(options?: RequestOptions, headers?: Headers) {
    headers = headers || new Headers({
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    options = options || new RequestOptions();
    options.headers = headers;
    return options;
  }

  private extractData(res: Response) {
    return res.json();
  }

  private handleError(error: Response | any) {
    const err = {
      error: error.json().error,
      msg: error.json().error.exception[0].message,
    };
    return Observable.throw(err);
  }
}

export class Dictionary {
  public static readonly DEVICE_STATES = [
    'SPRAWNY', 'ZEPSUTY'
  ];
  public static readonly DEVICE_CATEGORIES = [
    'LODOWKA', 'TELEWIZOR', 'LAPTOP', 'TELEFON'
  ];
}
